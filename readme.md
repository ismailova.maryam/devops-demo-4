# Demo-4 for DevOps course
## Setting CI/CD for Spring Petclinic on Kubernetes cluster

### Table of contents:
* [Tasks](#tasks)
* [Introduction](#introduction)
* [Structure](#structure)
* [Instruction](#instruction)

### Tasks


- [x] Select provider for Kubernetes cluster - GCP
- [x] Create a VPC and a Kubernetes cluster (VPC-Native)
- [x] Build Java image
- [x] Deploy Java image and a mysql database to Kubernetes cluster

- [x] A job triggered with commit pushed to repo (CI)
- [x] A job to deploy code to GKE
- [x] Healthcheck step

- Additional tasks:

- [x] Use Cloud SQL for database
- [ ] Deploy Kubernetes cluster manually

- Extra:

- [x] Secrets for sensitive data
- [x] ConfigMaps for application properties
- [x] Ingress controller instead of a LoadBalancer for every application

### Introduction

This is a demo to provision a Kubernetes cluster on GCP's GKE, and deploy applications on it in a CI/CD process. All using - Gitlab/Terraform/Helm/GCP.

### Structure

Terraform script provisions and configures the whole infrastructure.

0. Application is rebuilt and repackaged if there are any changes to source files
1. VPC is created with regard to VPC-Native GKE.
2. Kubernetes cluster is provisionned by GKE
3. Petclinic deployment with all required resources is deployed
    3.1 Context path of Application is changed to /app in order to use ingress. 
    3.2 A Healthcheck is set for petclinic. It is *important*, as it is used by ingress to check whether the backend is healthy. And when the context path is changed it works incorrectly.
4. Helm provider is used to deploy a kubernetes dashboard with helm chart.
    4.1 ReadOnly to all resources(except  secrets) access is enabled by default
5. An ingress controller is deployed. Which exposes the application behind: http://{LB_IP}/app/, and the kubernetes dashboard as http://{LB_IP}
    5.1 For ingress, both kubernetes dashboard and petclinic app are exposed through NodePort services.



### Instruction

Need to set:

1. GCLOUD_KEYFILE_JSON & GCLOUD_KEYFILE_JSON_VALUE

2. Several variables related to database connectivity:

    - TF_VAR_db_username
    - TF_VAR_db_password
    - TF_VAR_db_root_password
