resource "kubernetes_config_map" "petclinicconfig" {
  metadata {
    name = "petclinic-config"
  }

  data = {
    "application.properties"       = "${file("${path.cwd}/src/main/resources/application.properties")}"
    "application-mysql.properties" = "${file("${path.cwd}/src/main/resources/application-mysql.properties")}"
  }
}
