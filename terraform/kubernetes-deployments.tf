locals {
  petclinic_path = "/app"
}
resource "kubernetes_deployment" "petclinic" {
  metadata {
    name = "petclinic-java"
    labels = {
      App = "Petclinic"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        App = "Petclinic"
      }
    }
    template {
      metadata {
        labels = {
          App = "Petclinic"
        }
      }
      spec {

        image_pull_secrets {
          name = kubernetes_secret.image_registry.metadata[0].name
        }
        container {

          image = "${var.app_imagename}:${var.app_image_tag}"
          name  = "petclinic"
          port {
            container_port = 8080
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
          env {
            name = "MYSQL_PASS"
            value_from {
              secret_key_ref {
                key  = "password"
                name = kubernetes_secret.database.metadata.0.name
              }
            }
          }
          env {
            name = "MYSQL_USER"
            value_from {
              secret_key_ref {
                key  = "username"
                name = kubernetes_secret.database.metadata.0.name
              }
            }
          }

          # Change the context path of spring application to /app
          # Necessary to ensure Ingress on several applications
          env {
            name = "SERVER_SERVLET_CONTEXT_PATH"
            value = local.petclinic_path
          }
          env {
            name = "MYSQL_URL"
            # value  = "jdbc:mysql://${kubernetes_service.db-service.metadata.0.name}/${var.db_name}"
            value = "jdbc:mysql://${google_sql_database_instance.database.private_ip_address}/${var.db_name}"
          }
          volume_mount {
            mount_path = "/home/appuser/config/application.properties"
            name       = "petclinic-config"
            sub_path   = "application.properties"
          }
          volume_mount {
            mount_path = "/home/appuser/config/application-mysql.properties"
            name       = "petclinic-config"
            sub_path   = "application-mysql.properties"
          }

          liveness_probe {
            http_get {
              path = "/app/actuator/health"
              port = "8080"
            }
            initial_delay_seconds = "60"
            timeout_seconds  = "5"
            period_seconds = "5"
            failure_threshold = "5"
          }

          # This is important for setting ingress controller on GKE
          # LB used by ingress refers to probe checks from container for its healthchecks
          # if not set correctly(when context path is other than /), the application return 404 on /
          # and / is the default path used for healthchecks with GKE LB
          readiness_probe {
            http_get {
              path = "/app/actuator/health"
              port = "8080"
            }
            initial_delay_seconds = "60"
            timeout_seconds  = "5"
            period_seconds = "5"
            failure_threshold = "5"
          }

        }
        volume {
          name = "petclinic-config"
          config_map {
            name = kubernetes_config_map.petclinicconfig.metadata.0.name
          }
        }
      }
    }
  }

}
