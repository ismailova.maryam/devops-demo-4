terraform {
  backend "http" {}
  required_providers {
    google = {
      source = "hashicorp/google"
      # version = "3.52.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.1"
    }
  }
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
}
provider "google" {
  project = var.project_id
  region  = var.region
}

data "google_client_config" "default" {}