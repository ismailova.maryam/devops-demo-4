# VPC
resource "google_compute_network" "pet-vpc" {
  name                    = "${var.project_id}-petclinic-vpc"
  auto_create_subnetworks = "false"
}


# Subnet
resource "google_compute_subnetwork" "pet-subnet" {
  name          = "${var.project_id}-petclinic-subnet"
  region        = var.region
  network       = google_compute_network.pet-vpc.name
  ip_cidr_range = var.subnet_range

  # For VPC-native GKE clusters
  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = var.gke_services_range
  }

  secondary_ip_range {
    range_name    = "pod-ranges"
    ip_cidr_range = var.gke_pods_range
  }
}