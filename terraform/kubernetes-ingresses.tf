resource "kubernetes_ingress" "pet_ingress" {
  metadata {
    name = "pet-ingress"
  }

  spec {
    # backend {
    #     service_name = kubernetes_service.petclinic.metadata[0].name
    #     service_port = kubernetes_service.petclinic.spec[0].port[0].port
    # }
    rule {
      http {
          path {
            backend {
                service_name = kubernetes_service.petclinic.metadata[0].name
                service_port = kubernetes_service.petclinic.spec[0].port[0].port
            }
            path = "/app/*"
          }
          path {
            backend {
                service_name = local.helm_service_name
                service_port = local.helm_external_port
            }
            path = "/*"
          }
        }
    }

  }
  wait_for_load_balancer = true
  depends_on = [
      kubernetes_service.petclinic,
    #   because a kubernetes dashboard needs to be created before
      helm_release.kubedashboard
  ]
}
