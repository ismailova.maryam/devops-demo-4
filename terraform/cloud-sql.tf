resource "google_compute_global_address" "db_private_ip_address" {
  provider      = google-beta
  name          = "db-private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.pet-vpc.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = google_compute_network.pet-vpc.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.db_private_ip_address.name]
}

resource "google_sql_database_instance" "database" {
  name             = "petclinic-db-instance-${random_string.random.result}"
  database_version = "MYSQL_8_0"
  region           = var.region

  root_password       = var.db_root_password
  deletion_protection = "false"

  settings {
    disk_autoresize = false
    backup_configuration {
      enabled = false
    }
    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.pet-vpc.id
    }
    tier = var.gcp_db_type
  }

  depends_on = [google_service_networking_connection.private_vpc_connection]
}

resource "google_sql_database" "petclinic-database" {
  name     = var.db_name
  instance = google_sql_database_instance.database.name
}

resource "google_sql_user" "users" {
  name     = var.db_username
  instance = google_sql_database_instance.database.name
  password = var.db_password
}

# to create non-repetitive instance names for cloud sql
# https://stackoverflow.com/questions/59004844/terraform-google-sql-database-instance-not-being-created
resource "random_string" "random" {
  length  = 4
  special = false
  upper   = false

}