output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.petclinic-cluster.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.petclinic-cluster.endpoint
  description = "GKE Cluster Host"
}

# output "petclinic_lb_ip" {
#   value       = kubernetes_service.petclinic.status.0.load_balancer.0.ingress.0.ip
#   description = "IP the load balancer service for the application"
# }

output "ingress_lb_ip" {
  value       = kubernetes_ingress.pet_ingress.status[0].load_balancer[0].ingress[0].ip
  description = "IP of the Ingress"
}
output "cloudsql_connection" {
  description = "Connection string for Cloud SQL db"
  value = google_sql_database_instance.database.connection_name
}

output "cloudsql_privateip" {
  description = "Private IP for Cloud SQL db"
  value = google_sql_database_instance.database.private_ip_address
}
