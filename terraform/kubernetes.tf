provider "kubernetes" {
  host = google_container_cluster.petclinic-cluster.endpoint

  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(google_container_cluster.petclinic-cluster.master_auth[0].cluster_ca_certificate)
}
