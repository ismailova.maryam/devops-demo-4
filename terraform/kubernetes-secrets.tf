
resource "kubernetes_secret" "image_registry" {
  metadata {
    name = "docker-cfg"
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.CI_REGISTRY}": {
      "auth": "${base64encode("${var.CI_REGISTRY_USER}:${var.CI_REGISTRY_PASSWORD}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "database" {
  metadata {
    name = "petclinic-db"
  }
  data = {
    username      = "${var.db_username}"
    password      = "${var.db_password}"
    root_password = "${var.db_root_password}"
  }
}