locals {
  helm_service_name = "kd-kubernetes-dashboard"
  helm_service_type = "NodePort"
  helm_release_name = "kd"
  helm_external_port = "8081"
}
provider "helm" {
  kubernetes {
    host                   = google_container_cluster.petclinic-cluster.endpoint
    cluster_ca_certificate = base64decode(google_container_cluster.petclinic-cluster.master_auth[0].cluster_ca_certificate)
    token                  = data.google_client_config.default.access_token
  }
}

resource "helm_release" "kubedashboard" {
  name       = local.helm_release_name
  chart      = "kubernetes-dashboard"
  repository = "https://kubernetes.github.io/dashboard/"
  cleanup_on_fail = true
  wait = true

  set {
      name = "service.type"
      value = local.helm_service_type
  }
  set {
    name = "service.externalPort"
    value = local.helm_external_port
  }
  set {
    name = "protocolHttp"
    value = true
  }
  # Add read permission to all resources to the created  service-account
  set {
    name = "rbac.clusterReadOnlyRole"
    value = true
  }
}
