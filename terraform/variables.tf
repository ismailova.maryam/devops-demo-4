# GKE related variables
variable "project_id" {
  default     = "demo4devops"
  description = "project id"
}

variable "region" {
  default     = "europe-west1"
  description = "region"
}

variable "subnet_range" {
  default     = "10.40.0.0/16"
  description = "CIDR of main subnet "
}

variable "gke_pods_range" {
  default     = "10.41.0.0/16"
  description = "CIDR of main subnet "
}

variable "gke_services_range" {
  default     = "10.42.0.0/16"
  description = "CIDR of main subnet "
}

variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "gke_num_nodes" {
  default     = 2
  description = "number of gke nodes"
}

variable "gcp_db_type" {
  # default = "db-f1-micro"
  default     = "db-n1-standard-1"
  description = "cloud sql database instance type"
}

# Image related variables
variable "app_imagename" {
  # default = ""
  description = "The name of the image to use"
}
variable "app_image_tag" {
  description = "Tag for the docker image to use"
}

variable "db_imagename" {
  default     = "mysql"
  description = "The name of the image to use for database"
}

variable "db_image_tag" {
  default     = "8"
  description = "Tag for the docker image to use for db"
}

# database related variables
variable "db_name" {
  description = " The name of the database to create for petclinic"
  default     = "petclinic"
}
variable "db_username" {
  description = "Username for the database"
}

variable "db_password" {
  description = "Password for the database"
}

variable "db_root_password" {
  description = "Root password for the database"
}

# container registry related variables
variable "CI_REGISTRY" {
  description = "Name of the container registry"
}

variable "CI_REGISTRY_USER" {
  description = "Name of container egistry user"
}

variable "CI_REGISTRY_PASSWORD" {
  description = "Password of container registry user"
}