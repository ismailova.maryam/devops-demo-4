resource "kubernetes_service" "petclinic" {
  metadata {
    name = "petclinic-service"
  }
  spec {
    selector = {
      App = kubernetes_deployment.petclinic.spec.0.template.0.metadata[0].labels.App
    }
    port {
      port        = 8000
      target_port = 8080
    }

    type = "NodePort"
  }
}
