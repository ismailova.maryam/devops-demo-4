# escape=`

FROM openjdk:8-slim

RUN useradd -m -s /bin/bash appuser && usermod -aG sudo appuser `
     && mkdir /home/appuser/jars && mkdir /home/appuser/config `
     && chown appuser:appuser /home/appuser/jars && chown appuser:appuser /home/appuser/config

EXPOSE 8080

USER appuser

COPY --chown=appuser:appuser ./target/*.jar /home/appuser/jars
COPY --chown=appuser:appuser ./src/main/resources/application.properties /home/appuser/config/
COPY --chown=appuser:appuser ./src/main/resources/application-mysql.properties /home/appuser/config/

CMD java -jar -Dspring.profiles.active=mysql /home/appuser/jars/*.jar --spring.config.location=/home/appuser/config/
